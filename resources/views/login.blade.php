
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Đồ án 3: Trắc nghiệm</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/formlogin.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/jumbotron-narrow.css') }}" rel="stylesheet" type="text/css">

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="home">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Answer Quiz</h3>
        <br>
      </div>
      <div class="inner">

        <h1>Answer Quiz</h1>
        <h3>Chào mừng bạn đến với ứng dụng của chúng tôi</h3>

          <!-- @if($errors->has('txtUsername'))

              <p style="color: red">{{$errors ->first('txtUsername')}}</p>
          @endif
          @if($errors->has('txtPassword'))
              <p style="color: red">{{$errors->first('txtPassword')}}</p>
          @endif
          @if($errors && !$errors->has('txtUsername') && !$errors->has('txtPassword'))
              <p style="color: red">{{$errors ->first()}}</p>
          @endif -->
          @if(count($errors)>0)
            <div class="alert alert-danger">
              <strong>Error: </strong> There were some problems
              <ul>
                  @foreach($errors->all() as $error)
                    <li>{{ $error }} </li>
                  @endforeach
              </ul>
            </div>
          @endif

        <form action="{{route('login')}}" role="form" method="POST">
            <lagend>Login to your account</lagend><br>
            <label><span class="glyphicon glyphicon-user"></span></label>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>

            <input type="text" placeholder='Username' class="input" name="txtUsername" autofocus="" /><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>

            <input type="password" placeholder="password" class="input" name="txtPassword"/><br>
            <button type="submit" class="button" name="btnSignin">Sign in</button>
            <a href="{{route('register')}}"><button type="button" class="button">Register</button></a>
        </form>

    </div>

      <footer class="footer">
        <p>&copy; 2017 BachKhoa Company, Inc.</p><br>
        <p>Phạm Xuân Biển - facebook:<a href="https://www.facebook.com/bienpham224"> Biển Phạm </a></p>
      </footer>

    </div>
  </body>
</html>
