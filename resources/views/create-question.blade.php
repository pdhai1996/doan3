<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tạo Test</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/formlogin.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/jumbotron-narrow.css') }}" rel="stylesheet" type="text/css">

  </head>
  <body>
  	<div id="header">
  		
  	</div>

  	<div id="main">
  		<div class="container">
            <h1 class="page-header">
            Tạo bộ câu hỏi
            </h1>
  			<div class="col-md-6 ">

  				@if(count($errors)>0)
						   <div class="alert alert-danger">
						   	  @foreach($errors->all() as $er)
		                        {{$er}} <br>
						   	  @endforeach
						   </div>
						   @endif
				@if(Session::has('thongbao'))
				      <div class="alert alert-success">
						   	  {{Session::get('thongbao')}}
						   </div>
				@endif		  
  				<form action="createQuestion" method="post">
  				<input type="hidden" name="_token" value="{{csrf_token()}}">

  				  <div class="form-group">
				    <label class="control-label">Bộ câu hỏi:</label>
				    <select name="test_id" class="form-control" >
				       @foreach($data as $me)
				           <option value="{{ $me->test_id }}">{{ $me->title}}</option>
				       @endforeach
				  	</select>
				  </div>
				  
				  <div class="form-group">
				    <label class="control-label">Nội dung câu hỏi </label>
				    <input type="text" class="form-control" name="question">				    				    			   
				  </div>
				  <div class="form-group">
				    <label class="control-label">Đáp án A </label>
				    <input type="text" class="form-control" name="answer_a">
				  </div>
				  <div class="form-group">
				   <label class="control-label">Đáp án B </label>
				    <input type="text" class="form-control" name="answer_b">
				  </div>
				  <div class="form-group">
				  	<label class="control-label">Đáp án C </label>
				    <input type="text" class="form-control" name="answer_c">
				  </div>
				  <div class="form-group">
				  	 
				    <label class="control-label">Đáp án D</label>
				    <input type="text" class="form-control" name="answer_d">
				  </div>
				  <div class="form-group">
				  	 <label class="control-label">Đáp án đúng : </label>
				    <select name="correct_answer" class="form-control" >				    
				     <option value="1">A</option>
				     <option value="2">B</option>
				     <option value="3">C</option>
				     <option value="4">D</option>
				  	</select>
				  </div>
				  <div class="form-group">
				  	<label class="control-label">Thông tin thêm về đáp án</label>
				    <input type="text" class="form-control" name="content">
				  </div>
				  <button type="submit" class="btn btn-default">Gửi</button>
				  <button type="reset" class="btn btn-default">Reset</button>
				</form>
  			</div>
  		</div>
  	</div>
  </body>
</html>

<script src="http://code.jquery.com/jquery-latest.js"></script>

<script type="text/javascript" src="{{url('public/user/js/myscript.js')}}"></script>

