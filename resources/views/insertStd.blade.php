<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Insert</title>
  </head>
  <body>
      <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        @if($errors->has('txtId'))
            <p style="color: red">{{$errors->first('txtId')}}</p>
        @endif
        <input type="text" name="txtId" placeholder="writing id in here.." /><br>
        @if($errors->has('txtName'))
            <p style="color: red">{{$errors->first('txtName')}}</p>
        @endif
        <input type="text" name="txtName" placeholder="writing name in here.." /><br>
        <input type="text" name="txtClass" placeholder="writing class in here.." /><br>
        <input type="text" name="txtAdress" placeholder="writing adress in here.." /><br>
        <input type="submit" name="ok" value="Insert Student" />
      </form>
  </body>
</html>
