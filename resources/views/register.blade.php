
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Đồ án 3: Trắc nghiệm</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/formlogin.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/jumbotron-narrow.css') }}" rel="stylesheet" type="text/css">



  </head>

  <body style="background-image: url("bg.jpg")";>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="/">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Answer Quiz</h3>
        <br>
      </div>
      <div class="inner">

        <h1>Answer Quiz</h1>
        <h3>Chào mừng bạn đến với ứng dụng của chúng tôi</h3>
        <div class="col-lg-12">
        @if(count($errors)>0)
          <div id="error" class="alert alert-danger">
            <strong>Error: </strong> There were some problems
            <ul>
                @foreach($errors->all() as $error)
                  <li>{{ $error }} </li>
                @endforeach
            </ul>
          </div>
        @endif
      </div>

        <form action="{{route('register')}}" method="post" enctype="multipart/form-data">
            <lagend>Register new account</lagend><br>
            <label><span class="glyphicon glyphicon-user"></span></label>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <input type="text"  placeholder='Username' class="input" name="txtUsername" autofocus="" /><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <input type="password"  placeholder="password" class="input" name="txtPassword"/><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <input type="text"  placeholder="first_name" class="input" name="txtFirstname"/><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <input type="text"  placeholder="last_name" class="input" name="txtLastname"/><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <input type="text"  placeholder="email" class="input" name="txtEmail"/><br>
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <input type="text"  placeholder="phone number" class="input" name="txtPhonenumber"/><br>
            <button type="submit" class="button" name="btnRegister" onclick="hideError();">Register</button>

        </form>

    </div>

      <footer class="footer">
        <p>&copy; 2017 BachKhoa Company, Inc.</p><br>
        <p>Phạm Xuân Biển - facebook:<a href="https://www.facebook.com/bienpham224"> Biển Phạm </a></p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    <script src="{{ asset('public/js/myscript.js') }}"></script>
    <script>
      function hideError(){
        $("#error").hide(3000);
        $("div.alert").delay(3000).slideUp();
      }
    </script>
  </body>
</html>
