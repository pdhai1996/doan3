<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tạo Test</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/formlogin.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/jumbotron-narrow.css') }}" rel="stylesheet" type="text/css">

  </head>
  <body>
  	<div id="header">
  		
  	</div>

  	<div id="main">
  		<div class="container">
            <h1 class="page-header">
            Tạo bộ trắc nghiệm
            </h1>
  			<div class="col-md-6 ">
  			
						  	@if(count($errors)>0)
						   <div class="alert alert-danger">
						   	  @foreach($errors->all() as $er)
		                        {{$er}} <br>
						   	  @endforeach
						   </div>
						   @endif
						   						  
  				<form action="createTest" method="post">
  				<input type="hidden" name="_token" value="{{csrf_token()}}">
				  <div class="form-group">
				    <label class="control-label">Chọn chủ đề: </label>
				    <select name="topic_id" class="form-control" >
				     <option value="1" selected="selected">Lịch sử</option>
				     <option value="2">Địa lí</option>
				     <option value="3">Văn học</option>
				     <option value="6">Thể thao</option>
				     <option value="5">Khoa học -Xã hội</option>
				     <option value="4">Toán học</option>
				  	</select>
				  </div>
				  <div class="form-group">
				    <label  class="control-label">Đề mục bộ trắc nghiệm:</label>
				    <input type="text" class="form-control" name="title">
				  </div>
				  <div class="form-group">
				    <label class="control-label">Url ảnh</label>
				   <input type="text" class="form-control" name="image_url">
				  </div>
				 
				  <button type="submit" class="btn btn-default">Gửi</button>
				  <button type="reset" class="btn btn-default">Reset</button>
				  <button id="testjs">Test</button>
				</form>
  			</div>
  		</div>
  	</div>
  </body>
</html>

<script src="{{url('public/js/jquery-3.2.0.min.js')}}"></script>

<script type="text/javascript" src="{{url('public/user/js/myscript.js')}}"></script>