<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Insert</title>
  </head>
  <body>
      <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        @if($errors->has('txtId'))
            <p style="color: red">{{$errors->first('txtId')}}</p>
        @endif
        <input type="hidden" name="txtId" placeholder="writing id in here.."  /><br>
        @if($errors->has('txtName'))
            <p style="color: red">{{$errors->first('txtName')}}</p>
        @endif
        <input type="text" name="txtName" placeholder="writing name in here.." value="{{old('txtName',isset($data) ? $data['HoTen']:null)}}" /><br>
        <input type="text" name="txtClass" placeholder="writing class in here.." value="{{old('txtClass',isset($data) ? $data['LopHoc']:null)}}"/><br>
        <input type="text" name="txtAdress" placeholder="writing adress in here.." value="{{old('txtAdress',isset($data) ? $data['DiaChi']:null)}}" /><br>
        <input type="submit" name="ok" value="Insert Student" />
      </form>
  </body>
</html>
