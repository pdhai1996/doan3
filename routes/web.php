<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contain s the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'/','uses'=>'HomeController@getHome']);
Route::get('home',['as'=>'getHome','uses'=>'HomeController@getHome']);
Route::get('login',['as'=>'login','uses'=>'Auth\LoginController@getLogin']);
Route::post('login',['as'=>'login','uses'=>'Auth\LoginController@postLogin']);
Route::get('register',['as'=>'register','uses'=>'Auth\RegisterController@getRegister']);
Route::post('register',['as'=>'register','uses'=>'Auth\RegisterController@postRegister']);
Route::get('logout',['as'=>'logout','uses'=>'Auth\LoginController@logout']);
Route::any('{all?}','HomeController@getHome')->where('all', '(.*)');    // if you input wrong adress http, this page will go to home page

Route::group(['prefix'=>'admin'], function(){
  Route::group(['prefix'=>'cate'], function(){
    Route::get('list',['as'=>'admin.cate.getList','uses'=>'CateController@getList']);
    Route::get('add',['as'=>'admin.cate.getAdd','uses'=>'CateController@getAdd']);
    Route::post('add',['as'=>'admin.cate.postAdd','uses'=>'CateController@postAdd']);
    Route::get('delete/{id} ',['as'=>'admin.cate.getDelete','uses'=>'CateController@getDelete']);
    Route::get('edit/{id}',['as'=>'admin.cate.getEdit','uses'=>'CateController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.cate.postEdit','uses'=>'CateController@postEdit']);
  });
});
