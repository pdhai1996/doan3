<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class topic extends Model
{
    protected $table= "Topic";
    protected $fillable=['name','note','is_active'];
    public $timestamps= false;

    public function test(){
          return $this->hasMany('App\test');
    }
}