<?php

namespace App\Http\Controllers\User;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\hocsinh;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Validator;
use App\Http\Requests;

use App\test;
use App\question;
use App\detail_infor;
class CreateTestController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function getCreateTest(){
        return view('create-test');
    }

    public function postCreateTest(request $request){

        $this->validate($request,
          [
               'title'=>'required|min:20|max:200'
          ],
          [
               'title.required'=>'Bạn chưa nhập đề mục dữ liệu',
               'title.min'=>'Đề mục quá ngắn',
               'title.max'=>'Đề mục quá dài',
          ]);
        $test=new test;
        $test->topic_id=$request->topic_id;
        $test->user_id=1;
        $test->title=$request->title;
        $test->is_done=0;
        $test->image_url=$request->image_url;
        $test->save();
        $title =$request->title;
        return redirect()->route('createQuestion',$title);
        
    }
     
    public function set(){
         
        $data= test::select('test_id','title')->where('test_id',1)->get();
        return redirect()->route('get')->with(['thongbao'=>'ses']);
     }
     public function get(request $request){
          echo  $_COOKIE["vai"];
    }

    public function getCreateQuestion($title){
        $data= test::select('test_id','title')->where('title',$title)->get();
        return view('create-question', compact('data'));
    }

    public function postCreateQuestion(request $request){

         $this->validate($request,
          [
               'question'=>'required|min:5|max:200'
          ],
          [
               'question.required'=>'Bạn chưa nhập nội dung câu hỏi',
               'question.min'=>'Đề mục quá ngắn',
               'question.max'=>'Đề mục quá dài',
          ]);

        
        $question=new question;
        $detail_infor=new detail_infor;
        $question->test_id=$request->test_id;
        $question->question=$request->question;
        $question->answer_a=$request->answer_a;
        $question->answer_b=$request->answer_b;
        $question->answer_c=$request->answer_c;
        $question->answer_d=$request->answer_d;
        $question->correct_answer=$request->correct_answer;
        $question->number_a=0;
        $question->number_b=0;
        $question->number_c=0;
        $question->number_d=0;
        $question->save();
        $data= test::select('test_id','title')->where('test_id',$request->test_id)->get();
        foreach ($data as $dt) {
          $title =$dt->title;
        }
        
        return redirect()->route('createQuestion',$title)->with(['thongbao'=>'Success !! Completed']);
    }

}

