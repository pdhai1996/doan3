<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\http\Request;
use Illuminate\Support\MessageBag;
use App\Cart;
use App\Cates;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    public function getLogin(){
        return view('login');
    }

    public function logout(){
      Auth::logout();
      	return redirect('home');
    }
    public function postLogin(LoginRequest $req){
        $login = [
        'username' => $req->txtUsername,
        'password' => $req->txtPassword
        ];
        if(Auth::attempt($login)){
          $user = Auth::user();
          if($user->authority == 2){
            return redirect('home');
          }else{
            return redirect('login');
          }
        }else{
          $errors = new MessageBag(['errorLogin'=>'User or Password is wrong']);
          return redirect()->back()->withInput()->withErrors($errors);
        }
  }
}
